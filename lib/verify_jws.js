var jsrsasign = require('jsrsasign');
var jwt = require('jsonwebtoken');

module.exports  = function(token, secretOrKey, options, callback) {
    var isVerified = false;
    try {
      isVerified = jsrsasign.jws.JWS.verify(token, secretOrKey, ["RS256"]);
    } catch(ex) {
      return callback(ex, null);
    }

    // return an error when the token is not valid
    if (false === isVerified) {
      return callback(new Error('Invalid web token'), null);
    }

    // decode the webtoken to get the payload
    var decoded = jwt.decode(token, {complete: true});
    callback(null, decoded.payload);
};
